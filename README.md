# LLVM_Compiler_Optimization

> Built the LLVM dataflow analysis framework (**forward/backward** analysis and **the worklist algorithm**) in C++.

> Implemented lattice and flow function of **Reaching Definition Analysis** based on the data-flow analysis framework (forward analysis) using Functon Pass.
>
>  - Reaching Definition: For each point in the program, determine if each definiton in the program reaches the point.
>

> Implemented lattice and flow function of **Liveness Variables Analysis** based on the data-flow analysis framework (backward analysis) using Functon Pass.
>
>  - Liveness Variables: a variable is live at a particular point in the program if its value at that point will be used in the future.
>


## Intallation and building LLVM pass

Export your LLVM install directory to `$LLVM_HOME`.

Then 
```
mkdir build && cd build
cmake ../
make
```

## running the provided LLVM pass

opt -enable-new-pm=0 -load libDFA.so --pass-LA -f -analyze < input_for_riv.ll > /dev/null
opt -enable-new-pm=0 -load libDFA.so --pass-LA -f -analyze < input_for_riv.ll > /dev/null