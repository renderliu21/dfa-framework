
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

namespace {

struct testPass : public FunctionPass {
  
  static char ID;
  
  testPass() : FunctionPass(ID) {}

  //bool runOnFunction(Function &F) override {

  virtual bool runOnFunction(Function &F) {
    errs() << "Hello: ";
    errs().write_escaped(F.getName()) << '\n';
    return false;
  }
}; // end of struct TestPass
}  // end of anonymous namespace

char testPass::ID = 0;

static RegisterPass<testPass> X("testPass", "Developed to test LLVM and docker",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);
