#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"

#include <map>
#include <string>
#include <iterator>
#include <set>

#include <stdint.h>
#include <stdlib.h>
#include <iostream>

#include "DFA.h"

using namespace llvm;
using namespace std;

class ReachingInfo : public Info {

public:

	ReachingInfo() {}

    ReachingInfo(const ReachingInfo& other) : Info(other) {
        defs = other.defs;
    }
  
    ReachingInfo(std::set<unsigned> i) {
        defs = i;
    }

    std::set<unsigned> getInfo() {
        return defs;
    }

    void setInfo(std::set<unsigned> s) {
        defs = s;
    }

    /*
     * Print out the information
     */
    void print() {
        errs() << "reaching definitions: ";

        for (auto i : defs){
            errs() << i << ", ";
        }
        errs() << "\n";
    }

	/*
     * Compare two pieces of information
     */
    static bool equals(ReachingInfo * info1, ReachingInfo * info2) {
        return info1->defs == info2->defs;
    }

    /*
     * Join two pieces of information.
     * The third parameter points to the result.
     */
    static Info* join(ReachingInfo * info1, ReachingInfo * info2, ReachingInfo * result) {
    	result->defs.insert(info1->defs.begin(), info1->defs.end());
        result->defs.insert(info2->defs.begin(), info2->defs.end());
        return result;
    }

private:
	std::set<unsigned> defs;

};


class ReachingDefinitionAnalysis : public DataFlowAnalysis<ReachingInfo, true>  {

    public:
        ReachingDefinitionAnalysis(ReachingInfo & bottom, ReachingInfo & initialState) 
            : DataFlowAnalysis(bottom, initialState) {}

        ~ReachingDefinitionAnalysis() {}

        int getInstrCategory(Instruction* I) {

            set<string> cat1 = {"alloca", "load", "getelementptr", "icmp", "fcmp", "select"};
            set<string> cat2 = {"br", "switch", "store"};
            set<string> cat3 = {"phi"};

            string opcodeName(I->getOpcodeName()); 

            if (isa<PHINode>(I))
                return 3;
            //else if (I->isBinaryOp() || cat1.find(opcodeName)!=cat1.end())
            //    return 1;
            else if (I->isBinaryOp())
                return 1;
            else if (cat1.find(opcodeName)!=cat1.end())
                return 1;
            else if (cat2.find(opcodeName)!=cat2.end())
                return 2;
            else
                return -1;
        }

        void flowfunction(Instruction * I,
                        std::vector<unsigned> & IncomingEdges,
                        std::vector<unsigned> & OutgoingEdges,
                        std::vector<ReachingInfo *> & Infos) {

            // First, joining the incoming data flows
            unsigned curIdx = InstrToIndex[I];
            ReachingInfo* inInfos = new ReachingInfo();

            for (auto it : IncomingEdges) {
                Edge edge = std::make_pair(it, curIdx);
                ReachingInfo* in = EdgeToInfo[edge];
                ReachingInfo::join(inInfos, in, inInfos);
            }

            ReachingInfo* outInfos = new ReachingInfo(*inInfos);

            int cat = getInstrCategory(I);

            switch (cat)
            {
                case 1: {
                    // First Category: IR instructions that return a value (defines a variable)
                    // where curIdx is the index of the IR instruction, 
                    // which corresponds to the variable <result> being defined.
                    std::set<unsigned> infoSet_tmp = outInfos->getInfo();
                    infoSet_tmp.insert(curIdx);
                    outInfos->setInfo(infoSet_tmp);
                }
                break;

                case 2:
                    // Second Category: IR instructions that do not return a value
                    // do nothing 
                break;

                case 3: {
                    // Third Category: phi instructions
                    // add the set of all the indices of the phi instructions.
                    std::set<unsigned> phiSet;
                    BasicBlock* bb = I->getParent();
                    unsigned firstNonPHI = InstrToIndex[bb->getFirstNonPHI()];
                    for (unsigned i = curIdx; i < firstNonPHI; i++) {
                        phiSet.insert(i);
                    }
                    ReachingInfo::join(outInfos, new ReachingInfo(phiSet), outInfos);
                }
                break;

                default:
                 
                break;

            }

            // Third: add the newly computed information into the flow function result called Infos
            for (unsigned i = 0; i < OutgoingEdges.size(); i++) {
                Infos.push_back(outInfos);
            }
        }
};


namespace {

    struct ReachingDefinitionAnalysisPass : public FunctionPass {
        
        public:
            static char ID;

        ReachingDefinitionAnalysisPass() : FunctionPass(ID) {}

        bool runOnFunction(Function &F) override {

            errs() << "Function: " << F.getName() << "\n";

            ReachingInfo bottom;
            ReachingInfo initialState;

            ReachingDefinitionAnalysis *rda = new ReachingDefinitionAnalysis(bottom, initialState);
            rda->runWorklistAlgorithm(&F);
            rda->print();
            return true;
        }

        virtual void getAnalysisUsage(AnalysisUsage& AU) const override{
            AU.setPreservesCFG();
        }
      
    }; // end of struct ReachingDefinitionAnalysisPass
}  // end of anonymous namespace


char ReachingDefinitionAnalysisPass::ID = 0;
static RegisterPass<ReachingDefinitionAnalysisPass> X("pass-RDA", "ReachingDefinitionAnalysis",
                                                        false /* Only looks at CFG */,
                                                        false /* Analysis Pass */);
