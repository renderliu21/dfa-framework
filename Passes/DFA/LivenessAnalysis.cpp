#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/Statistic.h"

#include <map>
#include <string>
#include <iterator>
#include <set>

#include <stdint.h>
#include <stdlib.h>
#include <iostream>

#include "DFA.h"

using namespace llvm;
using namespace std;

#define DEBUG_TYPE "LivenessAnalysisPass"
STATISTIC(RemovedInstructionCount, "Number of useless assignments found.");

class LivenessInfo : public Info {

public:

	LivenessInfo() {}

    LivenessInfo(const LivenessInfo& other) : Info(other) {
        vars = other.vars;
    }
  
    LivenessInfo(std::set<unsigned> i) {
        vars = i;
    }

    std::set<unsigned> getInfo() {
        return vars;
    }

    void setInfo(std::set<unsigned> s) {
        vars = s;
    }

    /*
     * Print out the information
     */
    void print() {
        errs() << "live variables: ";

        for (auto i : vars){
            errs() << i << ", ";
        }
        errs() << "\n";
    }

	/*
     * Compare two pieces of information
     */
    static bool equals(LivenessInfo * info1, LivenessInfo * info2) {
        return info1->vars == info2->vars;
    }

    /*
     * Join two pieces of information.
     * The third parameter points to the result.
     */
    static Info* join(LivenessInfo * info1, LivenessInfo * info2, LivenessInfo * result) {
    	result->vars.insert(info1->vars.begin(), info1->vars.end());
        result->vars.insert(info2->vars.begin(), info2->vars.end());
        return result;
    }

private:
	std::set<unsigned> vars;

};


class LivenessAnalysis : public DataFlowAnalysis<LivenessInfo, false>  {

    public:
        LivenessAnalysis(LivenessInfo & bottom, LivenessInfo & initialState) 
            : DataFlowAnalysis(bottom, initialState) {}

        ~LivenessAnalysis() {}

        int getInstrCategory(Instruction* I) {

            set<string> cat1 = {"alloca", "load", "getelementptr", "icmp", "fcmp", "select", "ExtractElement", "ExtractValue"};
            set<string> cat2 = {"br", "switch", "store"};
            set<string> cat3 = {"phi"};

            string opcodeName(I->getOpcodeName()); 

            if (isa<PHINode>(I))
                return 3;
            else if (I->isBinaryOp())
                return 1;
            else if (cat1.find(opcodeName)!=cat1.end())
                return 1;
            else if (cat2.find(opcodeName)!=cat2.end())
                return 2;
            else
                return 2;
        }

        void flowfunction(Instruction * I,
                        std::vector<unsigned> & IncomingEdges,
                        std::vector<unsigned> & OutgoingEdges,
                        std::vector<LivenessInfo *> & Infos) {

            // First, joining the incoming data flows
            unsigned curIdx = InstrToIndex[I];
            LivenessInfo* inInfos = new LivenessInfo();

            for(auto it : IncomingEdges) {
                Edge edge = std::make_pair(it, curIdx);
                LivenessInfo* in = EdgeToInfo[edge];
                LivenessInfo::join(inInfos, in, inInfos);
            }

            LivenessInfo* outInfos = new LivenessInfo(*inInfos);

            int cat = getInstrCategory(I);

            switch (cat)
            {
                case 1: {
                    // First Category: IR instructions that return a value (defines a variable)
                    // where operands is the set of variables used (and therefore needed to be live) 
                    // in the body of the instruction, 
                    std::set<unsigned> infoSet_tmp = outInfos->getInfo();
                    for (Use &U:I->operands()) {
                        Value* V = U.get();
                        Instruction* inst = dyn_cast<Instruction>(V);
                        if(inst != nullptr) {
                            infoSet_tmp.insert(InstrToIndex[inst]);
                        }
                    }
                    // curIdx is the index of the IR instruction, 
                    // which corresponds to the variable <result> being defined.
                    infoSet_tmp.erase(curIdx);
                    outInfos->setInfo(infoSet_tmp);

                    // add the newly computed information into the flow function result called Infos
                    for (unsigned i = 0; i < OutgoingEdges.size(); i++) {
                        Infos.push_back(outInfos);
                    }
                }
                break;

                case 2: {
                    // Second Category: IR instructions that do not return a value
                    // Mostly the same as case I, except for no need to remove index of result
                    std::set<unsigned> infoSet_tmp = outInfos->getInfo();
                    for (Use &U:I->operands()) {
                        Value* V = U.get();
                        Instruction* inst = dyn_cast<Instruction>(V);
                        if(inst != nullptr) {
                            infoSet_tmp.insert(InstrToIndex[inst]);
                        }
                    }
                    outInfos->setInfo(infoSet_tmp);

                    // add the newly computed information into the flow function result called Infos
                    for (unsigned i = 0; i < OutgoingEdges.size(); i++) {
                        Infos.push_back(outInfos);
                    }
                }
                break;

                case 3: {

                    // Third Category: phi instructions
                    // 
                    std::set<unsigned> infoSet_tmp = outInfos->getInfo();

                    std::set<unsigned> phiSet;
                    BasicBlock* bb = I->getParent();
                    unsigned firstNonPHI = InstrToIndex[bb->getFirstNonPHI()];
                    
                    // remove all the index of result variable of phi instructions
                    for (unsigned i = curIdx; i < firstNonPHI; i++) {
                        infoSet_tmp.erase(i);
                    }

                    // each outgoing edge out[i], which originates from basic block p, 
                    // contains phi variables v_ij that are defined in its matching basic block p
                    for (size_t i = 0; i < OutgoingEdges.size(); i++) {

                        LivenessInfo* info_i = new LivenessInfo(infoSet_tmp);
                        std::set<unsigned> info_tmp = info_i->getInfo();

                        BasicBlock* outBB = IndexToInstr[OutgoingEdges[i]]->getParent();

                        for (unsigned index = curIdx; index < firstNonPHI; index++) {

                            Instruction* PHI_inst = IndexToInstr[index];
                            
                            for (Use &U:PHI_inst->operands()) {
                                Value* V = U.get();
                                Instruction* inst = dyn_cast<Instruction>(V);
                                if ((inst != nullptr) && (inst->getParent() == outBB)) {
                                    unsigned ind = InstrToIndex[inst];
                                    info_tmp.insert(ind);
                                    break;
                                }
                            }
                        }

                        info_i->setInfo(info_tmp);
                        Infos.push_back(info_i);
                    }
                }

                break;

                default:
                 
                break;

            }
        }

        std::set<unsigned> getUseless(Function* F)
        {
            std::set<unsigned> uselessInstrs;
            RemovedInstructionCount = 0;

            for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {

                Instruction * instr = &*I;
                unsigned curIdx = InstrToIndex[instr];

                bool useless = true;

                int cat = getInstrCategory(instr);

                if (cat != 1)
                    useless = false;
                else {
                    std::vector<unsigned> incoming_nodes;
                    getIncomingEdges(curIdx, &incoming_nodes);
                    
                    for (auto it : incoming_nodes) {
                        Edge edge = std::make_pair(it, curIdx);
                        LivenessInfo* i = EdgeToInfo[edge];
                        std::set<unsigned> vars = i->getInfo();
                        if (std::find(vars.begin(), vars.end(), curIdx) != vars.end()) {
                            useless = false;
                            break;
                        }
                    }
                }

                if (useless) {
                   uselessInstrs.insert(curIdx);
                }
            }

            // Delete the useless assignments! While also printing debug info.
            for (auto& idx: uselessInstrs) {
                errs() << "removing useless assignment %" << idx << "\n";

                RemovedInstructionCount++;
                
                // remove useless instructions from basicblock
                Instruction* instr = IndexToInstr[idx];
                instr->eraseFromParent();
            }
            errs() << "\n";

            return uselessInstrs;
        }
};

namespace {
    struct LivenessAnalysisPass : public FunctionPass {
        
        public:
            static char ID;

        LivenessAnalysisPass() : FunctionPass(ID) {}

        bool runOnFunction(Function &F) override {

            errs() << "Function: " << F.getName() << "\n";

            LivenessInfo bottom;
            LivenessInfo initialState;

            LivenessAnalysis *la = new LivenessAnalysis(bottom, initialState);
            la->runWorklistAlgorithm(&F);
            la->print();

            la->getUseless(&F);

            return true;
        }
      
    }; // end of struct ReachingDefinitionAnalysisPass
}  // end of anonymous namespace


char LivenessAnalysisPass::ID = 0;
static RegisterPass<LivenessAnalysisPass> X("pass-LA", "LivenessAnalysis",
                                            false /* Only looks at CFG */,
                                            false /* Analysis Pass */);
